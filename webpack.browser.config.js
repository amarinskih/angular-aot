const webpack = require('webpack');
const AngularCompilerPlugin = require('@ngtools/webpack').AngularCompilerPlugin;
const HtmlWebpackPlugin = require('html-webpack-plugin');
let path = require('path');

module.exports = (envOptions) => {
    envOptions = envOptions || {};
    const prod = envOptions.MODE === 'prod';
    const config = {
        entry: {
            main: './src/main.ts'
        },
        output: {
            path: path.join(__dirname, 'dist/browser'),
            filename: '[name].bundle.js',
        },
        resolve: {
            extensions: ['.ts', '.js', '.html'],
            // alias: {
            //     coreLib$: path.join(__dirname, '/src/app/custom-module.ts')
            // }
        },
        module: {
            rules: [
                { test: /\.html$/, use: 'html-loader'},
                { 
                    test: /\.css$/, 
                    use: ['to-string-loader', 'css-loader'] 
                },
                {         
                    test: /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/,
                    use: '@ngtools/webpack' 
                }
            ]
        },
        devtool: '#source-map',
        plugins: [
            new AngularCompilerPlugin({
                tsConfigPath: './tsconfig.json',
                entryModule: './src/app/app.module#AppModule',
            }),
            new HtmlWebpackPlugin({
                chunks: ['main'],
                template: './src/index.html'
              }),
        ],
        
    };
    return config;
};