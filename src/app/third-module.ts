import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseModule } from 'coreLib';

@Component({
    selector: "ab-third",
    template: "<h2>THIRD</h2><ab-component></ab-component>"
})
export class ThirdComponent {

}

@NgModule({
    imports: [CommonModule, BaseModule],
    declarations: [ThirdComponent],
    exports: [ThirdComponent]
})
export class ThirdModule {

}