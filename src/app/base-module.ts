import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseComponent } from './base-component';

@NgModule({
    imports: [CommonModule],
    declarations: [BaseComponent],
    exports: [BaseComponent],
})
export class BaseModule {

}
