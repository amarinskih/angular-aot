import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomComponent } from './custom-component';

@NgModule({
    imports: [CommonModule],
    declarations: [CustomComponent],
    exports: [CustomComponent],
})
export class BaseModule {

}

