import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
    selector: "ab-component",
    template: "<h2>BASE</h2><div (click)='click()'>{{counter}}</div>",
    moduleId: "base"
})
export class BaseComponent {
    public counter: number = 0;

    public click() {
        this.counter++;
    }
}
