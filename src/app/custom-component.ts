import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseComponent } from './base-component';

@Component({
    selector: "ab-component",
    template: "<h2>CUSTOM</h2><div (click)='click()'>{{100-counter}}</div>",
})
export class CustomComponent extends BaseComponent {

}
