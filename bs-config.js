
"use strict"
// Конфиг для Lite-server - https://github.com/johnpapa/lite-server
let proxy = require('http-proxy-middleware');

module.exports = {
    ui: false,
    logLevel: "silent",
    injectChanges: false, //Для CSS в Angular
    watchEvents: ["add", "change", "unlink", "addDir", "unlinkDir"],
    reloadOnRestart: true,
    reloadDelay: 2000,
    reloadDebounce: 200,
    port: 8080,
    files: ["./dist/browser/**/*"],
    server: {
        watchOptions: { // https://www.browsersync.io/docs/options#option-watchOptions
            ignoreInitial: false
        },
        baseDir: "./dist/browser",
    }
};