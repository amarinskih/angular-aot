const webpack = require('webpack');
const { AngularCompilerPlugin, PLATFORM } = require('@ngtools/webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
let path = require('path');
var nodeExternals = require('webpack-node-externals');

module.exports = (envOptions) => {
    envOptions = envOptions || {};
    const prod = envOptions.MODE === 'prod';
    const config = {
        entry: {
            main: './src/main.server.ts'
        },
        output: {
            path: path.join(__dirname, 'dist/ssr'),
            filename: '[name].bundle.js',
            libraryTarget: "commonjs",
        },
        resolve: {
            extensions: ['.ts', '.js', '.html'],
            // alias: {
            //     coreLib$: path.join(__dirname, '/src/app/custom-module.ts')
            // }
        },
        module: {
            rules: [
                { test: /\.html$/, use: 'html-loader' },
                {
                    test: /\.css$/,
                    use: ['to-string-loader', 'css-loader']
                },
                {
                    test: /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/,
                    use: '@ngtools/webpack'
                }
            ]
        },
        target: 'node',
        plugins: [
            new AngularCompilerPlugin({
                tsConfigPath: root('./tsconfig.server.json'),
                entryModule: root('./src/app/app.server.module#AppServerModule'),
                mainPath: root('./src/main.server.ts'),
                platform: PLATFORM.Server
            })
        ],
        externals: [nodeExternals()],
        node: {
            global: true,
            crypto: true,
            __dirname: true,
            __filename: true,
            process: true,
            Buffer: true
        }

    };
    return config;
};

function root(args) {
    args = Array.prototype.slice.call(arguments, 0);
    return path.join.apply(path, [__dirname].concat(args));
}